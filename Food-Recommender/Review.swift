//
//  Review.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/31/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

class Review{
    var author_name = ""
    var text = ""
    var author_image = ""
    var timestamp = ""
    var rating = 0.0
}
