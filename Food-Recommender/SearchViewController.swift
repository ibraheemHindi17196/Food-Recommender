//
//  SecondViewController.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/29/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import MapKit

class SearchViewController: UIViewController, UISearchBarDelegate, UISearchDisplayDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var search_bar: UISearchBar!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var no_results: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!

    var location_manager = CLLocationManager()
    var results = [SearchResult]()
    var in_call = false
    var request_url = ""
    var query = ""
    var lat = 0.0
    var long = 0.0
    var selected = SearchResult()
    var result_restaurant = Restaurant()
    
    
    override func viewWillAppear(_ animated: Bool) {
        no_results.alpha = 0
        search_bar.text = ""
        results.removeAll()
        table_view.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lat = UserDefaults.standard.object(forKey: "lat") as! Double
        long = UserDefaults.standard.object(forKey: "long") as! Double
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        search_bar.delegate = self

        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
    }

    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // --------------------- Setup search bar ---------------------

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        query = search_bar.text!
        if query != ""{
            request_url = "https://api.yelp.com/v3/autocomplete?text=\(searchText)&latitude=\(lat)&longitude=\(long)"
            
            safelyFetchResults()
        }
        else{
            results.removeAll()
            table_view.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search_bar.endEditing(true)
    }

    
    // --------------------- Setup table view ---------------------
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        let item = results[indexPath.row]
        cell.res_name.text = item.res_name
        cell.res_id = item.res_id
        
        return cell
    }
    
    // On selecting row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = results[indexPath.row]
        safelyFetchRestaurant()
    }
    

    // --------------------- Fetching results ---------------------
    
    func safelyFetchResults(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .default) {UIAlertAction in
                    self.safelyFetchResults()
                })
                network_alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in
                    self.results.removeAll()
                    self.table_view.reloadData()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                results.removeAll()
                table_view.reloadData()
                self.no_results.alpha = 0
                loader.startAnimating()
                self.fetchResults()
            }
        }
    }
    
    func fetchResults(){
        // Prepare request
        let url = URL(string: request_url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        request.setValue(app_delegate.token, forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
                self.endTask()
            }
            else {
                if data != nil {
                    do {
                        let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        DispatchQueue.main.async {
                            self.searchResultsFetched(json_response: json_response)
                        }
                    }
                    catch {
                        print("Error while fetching from \(self.request_url)")
                        self.endTask()
                    }
                }
                else{
                    self.endTask()
                }
            }
        }
        
        task.resume()
    }
    
    func searchResultsFetched(json_response: Dictionary<String, AnyObject>){
        endTask()
        
        if json_response.keys.contains("error"){
            let error = json_response["error"] as! Dictionary<String, AnyObject>
            let message = error["description"] as! String
            print("Error : \(message)")
            
            self.no_results.alpha = 1
        }
        else{
            if !json_response.keys.contains("businesses"){
                self.no_results.alpha = 1
            }
            else{
                let restaurants = json_response["businesses"] as! [Dictionary<String, AnyObject>]
                for item in restaurants{
                    let search_result = SearchResult()
                    search_result.res_id = item["id"] as! String
                    search_result.res_name = item["name"] as! String
                    
                    results.append(search_result)
                }
                
                self.table_view.reloadData()
                if restaurants.count == 0{
                    self.no_results.alpha = 1
                }
            }
        }
    }
    
    func safelyFetchRestaurant(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .default) {UIAlertAction in
                    self.safelyFetchRestaurant()
                })
                
                network_alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in})
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                loader.startAnimating()
                self.fetchRestaurant()
            }
        }
    }
    
    func fetchRestaurant(){
        // Prepare request
        let restaurant_url = "https://api.yelp.com/v3/businesses/\(selected.res_id)"
        let url = URL(string: restaurant_url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        request.setValue(app_delegate.token, forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
                self.endTask()
            }
            else {
                if data != nil {
                    do {
                        let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        DispatchQueue.main.async {
                            self.restaurantFetched(json_response: json_response)
                        }
                    }
                    catch {
                        print("Error while fetching from \(self.request_url)")
                        self.endTask()
                    }
                }
                else{
                    self.endTask()
                }
            }
        }
        
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
        if segue.identifier == "toSearchResult"{
            let destination = segue.destination as! RestaurantViewController
            destination.restaurant = result_restaurant
        }
    }
    
    func restaurantFetched(json_response: Dictionary<String, AnyObject>){
        endTask()
        
        if json_response.keys.contains("error"){
            print("Error : \(String(describing: json_response["error"]))")
            showSimpleAlert(title: "Error", message: "Something went wrong. Please try again later")
        }
        else{
            result_restaurant.id = json_response["id"] as! String
            result_restaurant.name = json_response["name"] as! String
            result_restaurant.image_url = json_response["image_url"] as! String
            result_restaurant.is_closed = json_response["is_closed"] as! Bool
            result_restaurant.rating = json_response["rating"] as! Double
            result_restaurant.price = json_response["price"] as! String
            
            let coordinates = json_response["coordinates"] as! Dictionary<String, Double>
            result_restaurant.latitude = coordinates["latitude"]!
            result_restaurant.longitude = coordinates["longitude"]!
            
            let location = json_response["location"] as! Dictionary<String, AnyObject>
            let address_parts = location["display_address"] as! [String]
            result_restaurant.display_address = address_parts.joined(separator: ", ")
            result_restaurant.display_phone = json_response["display_phone"] as! String
            result_restaurant.phone = json_response["phone"] as! String
            
            performSegue(withIdentifier: "toSearchResult", sender: self)
        }
    }
    
    func endTask(){
        loader.stopAnimating()
        in_call = false
    }
}






















