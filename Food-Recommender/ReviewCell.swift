//
//  ReviewCell.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/31/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var author_name: UILabel!
    @IBOutlet weak var author_image: UIImageView!
    @IBOutlet weak var review_text: UITextView!
    @IBOutlet weak var rating_view: UIStackView!
    @IBOutlet weak var timestamp: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
