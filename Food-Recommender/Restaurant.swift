//
//  Restaurant.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/29/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

class Restaurant{
    var id = ""
    var name = ""
    var image_url = ""
    var is_closed = false
    var rating = 0.0
    var latitude = 0.0
    var longitude = 0.0
    var price = ""
    var display_address = ""
    var phone = ""
    var display_phone = ""
    
    func toString() -> String{
        return "ID : \(id)" + "\n" +
               "Name : \(name)" + "\n" +
               "Image URL : \(image_url)" + "\n" +
               "Is Closed : \(is_closed)" + "\n" +
               "Rating : \(rating)" + "\n" +
               "Location : \(latitude), \(longitude)" + "\n" +
               "Price : \(price)" + "\n" +
               "Address : \(display_address)" + "\n" +
               "Phone : \(display_phone)" + "\n\n"
    }
}
