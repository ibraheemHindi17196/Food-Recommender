//
//  RestaurantCell.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/30/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {
    
    @IBOutlet weak var res_image: UIImageView!
    @IBOutlet weak var res_price: UILabel!
    @IBOutlet weak var res_name: UILabel!
    @IBOutlet weak var res_rating: UIStackView!
    @IBOutlet weak var is_open: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
