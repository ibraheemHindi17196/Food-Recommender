//
//  FirstViewController.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/29/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher

class NearbyViewController:
    UIViewController,
    CLLocationManagerDelegate,
    UITableViewDelegate,
    UITableViewDataSource{
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var no_results: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var location_manager = CLLocationManager()
    var request_url = ""
    var in_call = false
    var restaurants = [Restaurant]()
    var selected_restaurant = Restaurant()
    
    
    override func viewWillAppear(_ animated: Bool) {
        no_results.alpha = 0
        
        if let indexPath = table_view.indexPathForSelectedRow {
            table_view.deselectRow(at: indexPath, animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locateUser()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
        table_view.separatorColor = .black
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(willEnterForeground),
            name: .UIApplicationWillEnterForeground,
            object: nil
        )
    }
    
    @objc func willEnterForeground(){
        locateUser()
    }

    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    // On selecting row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected_restaurant = restaurants[indexPath.row]
        performSegue(withIdentifier: "toRestaurant", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
        
        if segue.identifier == "toRestaurant"{
            let destination = segue.destination as! RestaurantViewController
            destination.restaurant = selected_restaurant
        }
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell", for: indexPath) as! RestaurantCell
        
        if !in_call{
            let current_res = restaurants[indexPath.row]
            
            let url = URL(string: current_res.image_url)
            cell.res_image.kf.setImage(with: url)
            cell.res_price.text = current_res.price
            cell.res_name.text = current_res.name
            
            // Set rating
            var rating = current_res.rating
            let fraction = rating - Double(Int(rating))
            
            for item in cell.res_rating.subviews{
                if rating > 0{
                    let star_image = item as! UIImageView
                    
                    if floor(rating) > 0{
                        star_image.image = UIImage(named: "star")
                    }
                    else{
                        if fraction > 0{
                            star_image.image = UIImage(named: "half_star")
                        }
                    }
                    
                    rating -= 1
                }
            }
            
            // Set is open
            if current_res.is_closed{
                cell.is_open.text = "Closed Now"
                cell.is_open.textColor = .red
            }
            else{
                cell.is_open.text = "Open Now"
                cell.is_open.textColor = .green
            }
        }
        
        return cell
    }
    
    @objc func onAddressTapped(_ sender: AnyObject){
        let target_res = restaurants[sender.view.tag]
        let dir_url = URL(string: "http://maps.apple.com/maps?daddr=\(target_res.latitude),\(target_res.longitude)")!
        UIApplication.shared.open(dir_url, options: [:], completionHandler: nil)
    }
    
    @objc func onPhoneTapped(_ sender: AnyObject){
        let target_res = restaurants[sender.view.tag]
        phoneCall(phone: target_res.phone)
    }
    
    func phoneCall(phone: String)  {
        let url = URL(string: "TEL://\(phone)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    func locationError(message: String){
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel) {UIAlertAction in
            let settings_url = "App-Prefs:root=Privacy&path=LOCATION"
            UIApplication.shared.open(URL(string: settings_url)!, options: [:], completionHandler: nil)
        })
        
        self.present(alert, animated: true, completion: nil)
    }

    @objc func locateUser(){
        location_manager.delegate = self
        location_manager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            let status = CLLocationManager.authorizationStatus()
            
            if status == .denied || status == .restricted{
                self.locationError(message: "Please grant location access")
            }
            else if status == .notDetermined{
                location_manager.requestWhenInUseAuthorization()
            }
            else{
                location_manager.startUpdatingLocation()
            }
        }
        else {
            locationError(message: "Please enable location services")
        }
    }
    
    
    // Receive location updates in the following function :-
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let user_location = locations[0] as CLLocation
        location_manager.stopUpdatingLocation()

        let lat = user_location.coordinate.latitude
        let long = user_location.coordinate.longitude
        request_url = "https://api.yelp.com/v3/transactions/delivery/search?latitude=\(lat)&longitude=\(long)"
        
        UserDefaults.standard.set(lat, forKey: "lat")
        UserDefaults.standard.set(long, forKey: "long")
        UserDefaults.standard.synchronize()
        safelyFetchRestaurants()
    }
    
    func safelyFetchRestaurants(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .default) {UIAlertAction in
                    self.safelyFetchRestaurants()
                })
                network_alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in
                    self.restaurants.removeAll()
                    self.table_view.reloadData()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                restaurants.removeAll()
                loader.startAnimating()
                table_view.allowsSelection = false
                fetchRestaurants()
            }
        }
    }
    
    func fetchRestaurants(){
        // Prepare request
        let url = URL(string: request_url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        request.setValue(app_delegate.token, forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
                self.endTask()
            }
            else {
                if data != nil {
                    do {
                        let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        DispatchQueue.main.async {
                            self.onResponse(json_response: json_response)
                        }
                    }
                    catch {
                        print("Error while fetching from \(self.request_url)")
                        self.endTask()
                    }
                }
                else{
                    self.endTask()
                }
            }
        }
        
        task.resume()
    }
    
    func onResponse(json_response: Dictionary<String, AnyObject>){
        endTask()
        
        if json_response.keys.contains("error"){
            let error = json_response["error"] as! Dictionary<String, AnyObject>
            print(error)
            self.no_results.alpha = 1
        }
        else{
            let results = json_response["businesses"] as! [Dictionary<String, AnyObject>]
            
            for result in results{
                let restaurant = Restaurant()
                restaurant.id = result["id"] as! String
                restaurant.name = result["name"] as! String
                restaurant.image_url = result["image_url"] as! String
                restaurant.is_closed = result["is_closed"] as! Bool
                restaurant.rating = result["rating"] as! Double
                restaurant.price = result["price"] as! String
                
                let coordinates = result["coordinates"] as! Dictionary<String, Double>
                restaurant.latitude = coordinates["latitude"]!
                restaurant.longitude = coordinates["longitude"]!
                
                let location = result["location"] as! Dictionary<String, AnyObject>
                let address_parts = location["display_address"] as! [String]
                restaurant.display_address = address_parts.joined(separator: ", ")
                restaurant.display_phone = result["display_phone"] as! String
                restaurant.phone = result["phone"] as! String
                
                restaurants.append(restaurant)
            }
            self.table_view.reloadData()
            
            if restaurants.count == 0{
                self.no_results.alpha = 1
            }
        }
    }

    func endTask(){
        loader.stopAnimating()
        in_call = false
        table_view.allowsSelection = true
    }
}

