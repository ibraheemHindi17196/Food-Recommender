//
//  RestaurantViewController.swift
//  Food-Recommender
//
//  Created by Ibraheem Hindi on 8/31/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Kingfisher

class RestaurantViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var rating_view: UIStackView!
    @IBOutlet weak var open_label: UILabel!
    @IBOutlet weak var address_label: UILabel!
    @IBOutlet weak var phone_label: UILabel!
    @IBOutlet weak var address_icon: UIImageView!
    @IBOutlet weak var phone_icon: UIImageView!
    @IBOutlet weak var reviews_label: UILabel!
    @IBOutlet weak var reviews_table: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var restaurant = Restaurant()
    var reviews = [Review]()
    var in_call = false
    
    
    override func viewDidAppear(_ animated: Bool) {
        safelyFetchReviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set name
        name_label.text = restaurant.name
        
        // Set image
        let url = URL(string: restaurant.image_url)
        image_view.kf.setImage(with: url)
        
        // Set rating
        var rating = restaurant.rating
        let fraction = rating - Double(Int(rating))
        
        for item in rating_view.subviews{
            if rating > 0{
                let star_image = item as! UIImageView
                
                if floor(rating) > 0{
                    star_image.image = UIImage(named: "star")
                }
                else{
                    if fraction > 0{
                        star_image.image = UIImage(named: "half_star")
                    }
                }
                
                rating -= 1
            }
        }
        
        // Set open status
        if restaurant.is_closed{
            open_label.text = "Closed Now"
            open_label.textColor = .red
        }
        else{
            open_label.text = "Open Now"
            open_label.textColor = .green
        }
        
        // Set address and phone
        address_label.text = restaurant.display_address
        phone_label.text = restaurant.display_phone
        
        // Setup navigation listener
        address_icon.isUserInteractionEnabled = true
        let address_recognizer = UITapGestureRecognizer(target: self, action: #selector(onAddressTap))
        address_icon.addGestureRecognizer(address_recognizer)
        
        // Setup phone listener
        phone_icon.isUserInteractionEnabled = true
        let phone_recognizer = UITapGestureRecognizer(target: self, action: #selector(onPhoneTap))
        phone_icon.addGestureRecognizer(phone_recognizer)
        
        // Setup reviews table
        reviews_table.dataSource = self
        reviews_table.delegate = self
        reviews_table.tableFooterView = UIView()
        
    }
    
    @objc func onAddressTap(){
        let dir_url = URL(string: "http://maps.apple.com/maps?daddr=\(restaurant.latitude),\(restaurant.longitude)")!
        UIApplication.shared.open(dir_url, options: [:], completionHandler: nil)
    }
    
    @objc func onPhoneTap(){
        UIApplication.shared.open(
            URL(string: "tel://" + restaurant.phone)!,
            options: [:],
            completionHandler: nil
        )
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }

    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
        let current_review = reviews[indexPath.row]
        
        // Set author name and review text
        cell.author_name.text = current_review.author_name
        cell.review_text.text = current_review.text

        // Make author's image round and set it
        cell.author_image.layer.cornerRadius = cell.author_image.frame.size.width / 2
        cell.author_image.clipsToBounds = true
        
        if current_review.author_image != ""{
            let url = URL(string: current_review.author_image)
            cell.author_image.kf.setImage(with: url)
        }
        else{
            cell.author_image.image = UIImage(named: "user")
        }
        
        // Set rating
        var rating = current_review.rating
        let fraction = rating - Double(Int(rating))
        
        for item in cell.rating_view.subviews{
            if rating > 0{
                let star_image = item as! UIImageView
                
                if floor(rating) > 0{
                    star_image.image = UIImage(named: "star")
                }
                else{
                    if fraction > 0{
                        star_image.image = UIImage(named: "half_star")
                    }
                }
                
                rating -= 1
            }
        }
        
        // Set timestamp
        cell.timestamp.text = current_review.timestamp
        
        return cell
    }
    
    func safelyFetchReviews(){
        if !in_call{
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            if !app_delegate.isConnected(){
                let network_alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: .alert
                )
                
                network_alert.addAction(UIAlertAction(title: "Retry", style: .default) {UIAlertAction in
                    self.safelyFetchReviews()
                })
                network_alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in
                    self.reviews.removeAll()
                    self.reviews_table.reloadData()
                })
                
                self.present(network_alert, animated: true, completion: nil)
            }
            else{
                in_call = true
                reviews.removeAll()
                loader.startAnimating()
                self.fetchReviews()
            }
        }
    }
    
    func fetchReviews(){
        // Prepare request
        let request_url = "https://api.yelp.com/v3/businesses/\(restaurant.id)/reviews"
        let url = URL(string: request_url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        request.setValue(app_delegate.token, forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                self.showSimpleAlert(title: "Error", message: error!.localizedDescription)
                self.endTask()
            }
            else {
                if data != nil {
                    do {
                        let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        DispatchQueue.main.async {
                            self.onResponse(json_response: json_response)
                        }
                    }
                    catch {
                        print("Error while fetching from \(request_url)")
                        self.endTask()
                    }
                }
                else{
                    self.endTask()
                }
            }
        }
        
        task.resume()
    }
    
    func onResponse(json_response: Dictionary<String, AnyObject>){
        endTask()
        
        if json_response.keys.contains("error"){
            let error = json_response["error"] as! Dictionary<String, AnyObject>
            let message = error["description"] as! String
            print("Error : \(message)")
        }
        else{
            let results = json_response["reviews"] as! [Dictionary<String, AnyObject>]
            
            for result in results{
                let review = Review()
                let user = result["user"] as! Dictionary<String, AnyObject>
                
                if let image_url = user["image_url"] as? String{
                    review.author_image = image_url
                }
                
                review.author_name = user["name"] as! String
                review.rating = result["rating"] as! Double
                review.text = result["text"] as! String
                review.timestamp = result["time_created"] as! String
                self.reviews.append(review)
            }
            
            self.reviews_label.text = "Reviews (\(reviews.count))"
            self.reviews_table.reloadData()
        }
    }
    
    func endTask(){
        loader.stopAnimating()
        in_call = false
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}











