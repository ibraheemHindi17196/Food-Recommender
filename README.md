# Food-Recommender

A food app that lets users view nearby restaurants, details and reviews of any restaurant, and search for restaurants which takes the user location into consideration. It relies on Yelp's API.

Demo Video : https://www.dropbox.com/s/k0tmbte462co4oi/Food-Recommender.mp4?dl=0